#version 450 core

layout (location = 0) in vec3 gPosition;
out vec3 gPosNormalized; 

struct VoxelGrid {
	vec3 center;
	float dx;
	float extent;
	float extentInv;
};

uniform VoxelGrid gVoxelGrid;

void main()
{
	vec3 posGrid = gPosition - gVoxelGrid.center;
	vec3 posClip = posGrid * gVoxelGrid.extentInv;  
	gPosNormalized = 0.5 * (posClip + 1.0);
	gl_Position = vec4(posClip, 1.0);
}
