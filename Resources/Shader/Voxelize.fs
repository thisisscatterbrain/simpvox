#version 450 core

in vec3 gPosNormalized;
layout(r32f, binding = 0) uniform image3D gVoxels;

struct VoxelGrid {
	vec3 center;
	float dx;
	float extent;
	float extentInv;
};

uniform VoxelGrid gVoxelGrid;

//-----------------------------------------------------------------------------
ivec3 GetGridCoordFromPosition()
{
	ivec3 coord;
	coord.x = int(gPosNormalized.x / gVoxelGrid.dx);
	coord.y = int(gPosNormalized.y / gVoxelGrid.dx);
	coord.z = int(gPosNormalized.z / gVoxelGrid.dx);
	return coord;
}
//-----------------------------------------------------------------------------
void main()
{
	ivec3 c = GetGridCoordFromPosition();
	imageStore(gVoxels, c, vec4(1.0));
}
//-----------------------------------------------------------------------------