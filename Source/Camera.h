#ifndef CAMERA_H
#define CAMERA_H

#include "Fbx.h"
#include "ClassDecls.h"

class Camera {
	DECL_SINGLETON(Camera)
public:

	void Init(const FbxCamera& camera);

	// Setters
	void SetNear(float near);
	void SetFar(float far);

	// Sets field of view; [fovy] is in DEGREES.
	void SetFovy(float fovy);
	void SetAspect(float asp);

	void SetPosition(const glm::vec3& position);
	void SetCenter(const glm::vec3& center);
	void SetUp(const glm::vec3& up);

	// Getters
	const glm::mat4* GetPerspective() const;
	const glm::mat4* GetView() const;
	const glm::vec3* GetPosition() const { return &mPosition; }
	float GetNear() const { return mNear; }
	float GetFar() const { return mFar; }

	// Camera motion

	// Translates the camera according to [translation].
	void Translate(const glm::vec3& translation);

	// Moves the camera along its u axis.
	void MoveU(float x);

	// Moves the camera along its n axis.
	void MoveN(float x);

	// Rotates around the y axis.
	void RotateY(float angle);


private:

	// Computes perspective and view transforms for the current camera 
	// state.
	inline void ComputeTransforms() const;

	mutable glm::mat4 mPerspective;
	mutable glm::mat4 mView;
	
	glm::vec3 mPosition;
	mutable glm::vec3 mCenter;
	glm::vec3 mUp;

	float mNear;
	float mFar;
	float mFovy;
	float mAsp;

	// Indicates if the camera matrices need to be recomputed.
	mutable bool mIsDirty;
};


#endif