#include "GLProgram.h"
#include <glm/gtc/type_ptr.hpp>

//-----------------------------------------------------------------------------
static char* ReadFile(const char* filename)
{
	// read contents of a file and return a zero terminated string or 
	// NULL if the file could not be opened.

	FILE* f = NULL;
	size_t numChars = 1; /* # of chars in the file */
	char* contents = NULL;
	char ch = 0;
	size_t count = 0;

	f = fopen(filename, "r");

	if (!f)
	{
		return NULL;
	}

	/* count # of chars in the file */
	while (EOF != fgetc(f))
	{
		numChars++;
	}

	contents = (char*)malloc(numChars);

	if (!contents)
	{
		fclose(f);
		return NULL;
	}

	/* rewind and load the file's contents */
	rewind(f);

	while (1)
	{
		ch = fgetc(f);

		/* break if end of file is reached, don't forget to finish the string */
		if (ch == EOF)
		{
			contents[count] = '\0';
			break;
		}

		contents[count] = ch;
		count++;
	}

	/* clean up and return the contents */
	fclose(f);

	return contents;
}
//-----------------------------------------------------------------------------
static int AttachShaderWithSource(GLuint program, GLenum type,
	const char* source)
{
	GLuint shader = 0;
	GLint status;
	GLint infoLogLength; /* length of the info log */
	GLchar* infoLog; 	 /* the info log */

	shader = glCreateShader(type);

	if (!shader)
	{
		FAIL("Failed to create shader");
		return 0;
	}

	glShaderSource(shader, 1, &source, NULL);
	ASSERT(GL_NO_ERROR == glGetError());
	glCompileShader(shader);
	ASSERT(GL_NO_ERROR == glGetError());

		// check if the shader compiled correctly
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	ASSERT(GL_NO_ERROR == glGetError());

	// complain if something went wrong 
	if (status == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		infoLog = (GLchar*)malloc(infoLogLength + 1);
		glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);
		FAIL(infoLog);
		free(infoLog);
		glDeleteShader(shader);
		return 0;
	}

	glAttachShader(program, shader);
	ASSERT(GL_NO_ERROR == glGetError());
	return 1;
}
//-----------------------------------------------------------------------------
static int AttachShaderWithFile(GLuint program, GLenum type,
	const char* file)
{
	// Attach shader from file
	char* contents = NULL; /* contents of the file */
	int succ = 0;

	contents = ReadFile(file);

	if (!contents) {
		FAIL("Could not load shader file");
		return 0;
	}

	succ = AttachShaderWithSource(program, type, contents);
	ASSERT(NULL != contents);

	/* clean up */
	free(contents);
	return succ;
}
//-----------------------------------------------------------------------------
static GLenum GetTypeFromFilename(const char* filename)
{
	if (strstr(filename, ".vs")) {
		return GL_VERTEX_SHADER;
	}
	else if (strstr(filename, ".fs")) {
		return GL_FRAGMENT_SHADER;
	}
	else if (strstr(filename, ".gs")) {
		return GL_GEOMETRY_SHADER;
	}

	FAIL("Unsupported shader type");
}
//-----------------------------------------------------------------------------
static int LinkProgram(GLuint program)
{
	GLint status = 0;
	GLint infoLogLength; /* length of the info log */
	GLchar* infoLog; 	 /* the info log */
	GLint numShaders; 	 /* # of shader attached to the program */
	GLuint* shaders;     /* shaders attached to the program */
	int i = 0;

	// link the program
	glLinkProgram(program);

	// check if everything went well
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	// complain if s.th went wrong
	if (status == GL_FALSE)
	{
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
		infoLog = (char*)malloc(infoLogLength + 1);
		glGetProgramInfoLog(program, infoLogLength, NULL, infoLog);
		FAIL(infoLog);
		free(infoLog);
		return 0;
	}

	// detach and delete the shaders from the program
	glGetProgramiv(program, GL_ATTACHED_SHADERS, &numShaders);
	shaders = (GLuint*)malloc(sizeof(GLuint)*numShaders);
	glGetAttachedShaders(program, numShaders, NULL, shaders);

	for (i = 0; i < numShaders; i++) {
		glDetachShader(program, shaders[i]);
		glDeleteShader(shaders[i]);
	}

	return 1;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void GLProgram::InitFromFiles(const std::vector<std::string>& files)
{
	handle = glCreateProgram();

	for (const std::string& filename : files) {
		GLenum type = GetTypeFromFilename(filename.c_str());
		AttachShaderWithFile(handle, type, filename.c_str());
	}

	LinkProgram(handle);
}
//-----------------------------------------------------------------------------
void GLProgram::Deinit()
{
	glDeleteProgram(handle);
	handle = 0;
}
//-----------------------------------------------------------------------------
void GLProgram::Bind() const
{
	glUseProgram(handle);
}
//-----------------------------------------------------------------------------
bool GLProgram::IsBound() const
{
	GLint prog;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prog);
	return (handle == prog);
}
//-----------------------------------------------------------------------------
void GLProgram::SetUniform(const std::string& name, GLint i)
{
	ASSERT(IsBound())
	GLint loc = GetUniformLocation(name);
	glUniform1i(loc, i);
}
//-----------------------------------------------------------------------------
void GLProgram::SetUniform(const std::string& name, GLfloat f)
{
	ASSERT(IsBound())
	GLint loc = GetUniformLocation(name);
	glUniform1f(loc, f);
}
//-----------------------------------------------------------------------------
void GLProgram::SetUniform(const std::string& name, const glm::vec3& v3)
{
	ASSERT(IsBound())
	GLint loc = GetUniformLocation(name.c_str());
	glUniform3f(loc, v3[0], v3[1], v3[2]);
}
//-----------------------------------------------------------------------------
void GLProgram::SetUniform(const std::string& name, const glm::mat4& mat4)
{
	ASSERT(IsBound())
	GLint loc = GetUniformLocation(name);
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(mat4));
}
//-----------------------------------------------------------------------------
GLint GLProgram::GetUniformLocation(const std::string& name)
{
	ASSERT(IsBound());
	return glGetUniformLocation(handle, name.c_str());
}
//-----------------------------------------------------------------------------
