#ifndef LOG_H
#define LOG_H

#include <cstdarg>
#include <cstdio>

void SetFile(FILE* file);

void Reportf(const char* filename, int line,
	const char* format, ...);

#define LOG(FORMAT, ...)									\
	Reportf(__FILE__, __LINE__, (FORMAT), __VA_ARGS__);

#define LOG_IF( CONDITION, FORMAT, ...)					\
	if (!(CONDITION)) {										\
		Reportf(__FILE__, __LINE__, (FORMAT), __VA_ARGS__);	\
		DEBUG_BREAK											\
	}

#endif