#include "Voxelize.h"
#include "VoxelGrid.h"
#include "GLProgram.h"
#include "FullscreenQuad.h"
#include "DeviceSurface.h"

static GLProgram gProgram;

//-----------------------------------------------------------------------------
void Voxelize::Init()
{
	std::vector<std::string> srcFiles = {
		"../Resources/Shader/Voxelize.vs",
		"../Resources/Shader/Voxelize.fs"
	};

	gProgram.InitFromFiles(srcFiles);
	gProgram.Bind();
	gProgram.SetUniform("gVoxelGrid.center", gVoxelGrid.center);
	gProgram.SetUniform("gVoxelGrid.extent", gVoxelGrid.extent);
	gProgram.SetUniform("gVoxelGrid.extentInv", 1.0f / gVoxelGrid.extent);
	gProgram.SetUniform("gVoxelGrid.dx", 1.0f / float(gVoxelGrid.cellCount));
	GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
void Voxelize::Deinit()
{
	gProgram.Deinit();
}
//-----------------------------------------------------------------------------
void Voxelize::Execute()
{
	glDisable(GL_DEPTH_TEST);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glViewport(0, 0, gVoxelGrid.cellCount, gVoxelGrid.cellCount);
	gProgram.Bind();
//	gVoxelGrid.texture.Bind(0);
	gVoxelGrid.texture.BindImage(0, GL_WRITE_ONLY, GL_R32F);	

	for (DeviceSurface& surface : DeviceSurface::pool) {
		surface.Draw(GL_TRIANGLES);
	}

	GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
