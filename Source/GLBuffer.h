#ifndef GLBUFFER_H
#define GLBUFFER_H

#include "GLResource.h"

template <GLenum TARGET>
struct GLBuffer : public GLResource {
	void Init();
	void SetBufferData(GLsizei size, const GLvoid* data, GLenum usage);
	void Deinit();
	void Bind() const;
};

using GLArrayBuffer = GLBuffer<GL_ARRAY_BUFFER>;

// A utility buffer for defining vertex attributes.
template <GLuint INDEX, GLint DIM, GLenum TYPE>
struct GLAttribBuffer : public GLBuffer<GL_ARRAY_BUFFER> {};

// Attribute to index convention.
#define GL_VERT_ATTRIB_POS 0
#define GL_VERT_ATTRIB_NORM 1
#define GL_VERT_ATTRIB_UV 2

using GLAttribBufferPos3f = GLAttribBuffer<GL_VERT_ATTRIB_POS, 3, GL_FLOAT>;
using GLAttribBufferPos2f = GLAttribBuffer<GL_VERT_ATTRIB_POS, 2, GL_FLOAT>;
using GLAttribBufferNorm3f = GLAttribBuffer<GL_VERT_ATTRIB_NORM, 3, GL_FLOAT>;

#include "GLBuffer.inl"

#endif