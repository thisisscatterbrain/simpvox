#ifndef APPLICATIONSETTINGS_H
#define APPLICATIONSETTINGS_H

#include "ClassDecls.h"
#include "Common.h"
#include <string>

class ApplicationSettings {
	DECL_SINGLETON(ApplicationSettings)
public:

	RO_MEMBER(I32, WindowWidth);
	RO_MEMBER(I32, WindowHeight);
	RO_MEMBER(std::string, WindowTitle);

	// Loads default settings for the application.
	void Load();
};

#endif