template <GLuint INDEX, GLint DIM, GLenum TYPE>
void GLVertexArray::SetVertexAttribute(
	const GLAttribBuffer<INDEX, DIM, TYPE>& buffer)
{
	glBindVertexArray(handle);
	buffer.Bind();
	glEnableVertexAttribArray(INDEX);
	glVertexAttribPointer(INDEX, DIM, TYPE, GL_FALSE, 0, 0);
}