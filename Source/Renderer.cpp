#include "Renderer.h"
#include "GLProgram.h"
#include "FullscreenQuad.h"
#include "VoxelGrid.h"
#include "Voxelize.h"
#include "DeviceSurface.h"
#include "Mesh.h"
#include <cstdio>

Renderer Renderer::instance;
static GLProgram gProgram;

//-----------------------------------------------------------------------------
Renderer::Renderer()
{
}
//-----------------------------------------------------------------------------
Renderer::~Renderer()
{
}
//-----------------------------------------------------------------------------
void Renderer::Init()
{
	static const std::vector<std::string> srcFiles = {
		"../Resources/Shader/FullscreenQuad.vs",
		"../Resources/Shader/FullscreenQuad.fs",
	};

	gVoxelGrid.Init(glm::vec3(0.0, 0.0, 0.0), 2.0, 32, GL_R32F);
	gProgram.InitFromFiles(srcFiles);
	gFullscreenQuad.Init();
	Voxelize::Init();

	for (Mesh& mesh : Mesh::pool) {
		new DeviceSurface(mesh);
	}
}
//-----------------------------------------------------------------------------
void Renderer::Deinit()
{
	Voxelize::Deinit();
	gVoxelGrid.Deinit();
	gProgram.Deinit();
	gFullscreenQuad.Deinit();
}
//-----------------------------------------------------------------------------
void Renderer::OnUpdate(float dt)
{
	Voxelize::Execute();
	GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
