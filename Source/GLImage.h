#ifndef GLIMAGE_H
#define GLIMAGE_H

#include "Common.h"
#include "OpenGL.h"

struct GLImage {
	static U32 GetPixelSize(GLuint format, GLuint type);
	static U32 GetChannelCount(GLuint format);
};

#endif