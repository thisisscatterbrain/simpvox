#ifndef ASSERT_H
#define ASSERT_H

#include "Log.h"

#ifdef _MSC_VER
	#define DEBUG_BREAK __debugbreak();
#else
	#define DEBUG_BREAK
#endif

#define ASSERT( CONDITION )									\
	if (!(CONDITION)) {										\
		Reportf(__FILE__, __LINE__, "Assertion %s failed.", #CONDITION);				\
		DEBUG_BREAK											\
	}

#define FAIL(FORMAT, ...) do {									\
		Reportf(__FILE__, __LINE__, (FORMAT), __VA_ARGS__);	\
		DEBUG_BREAK } while(0)


 
#endif