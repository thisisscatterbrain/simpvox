#include "GLResource.h"
#include "Log.h"

//-----------------------------------------------------------------------------
GLResource::GLResource()
	:
	handle(0)
{
}
//-----------------------------------------------------------------------------
GLResource::~GLResource()
{
	// The resource should have been deleted before reaching here. To indicate
	// this the handle should have been set to zero.
	if (handle) {
		LOG("Warning: GL resource %d potentially not deleted.", handle);
	}
}
//-----------------------------------------------------------------------------