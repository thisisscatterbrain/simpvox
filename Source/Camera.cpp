#include "Camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

Camera Camera::instance;

//-----------------------------------------------------------------------------
Camera::Camera() 
	:
	mPosition(0.0f, 0.0f, 0.0f),
	mCenter(0.0f, 0.0f, -1.0f),
	mUp(0.0f, 1.0f, 0.0f),
	mNear(0.01f),
	mFar(100.0f),
	mFovy(glm::radians(60.0f)),
	mAsp(1280.0f / 720.0f),
	mIsDirty(true)
{
}
//-----------------------------------------------------------------------------
Camera::~Camera() 
{
}
//-----------------------------------------------------------------------------
void Camera::Init(const FbxCamera& camera)
{
	SetFovy(camera.FieldOfViewY.Get());
	SetNear(camera.NearPlane.Get());
	SetFar(camera.FarPlane.Get());
	SetPosition(GlmVec3FromFbxDouble3(camera.Position.Get()));
	SetCenter(GlmVec3FromFbxDouble3(camera.InterestPosition.Get()));
	SetUp(GlmVec3FromFbxDouble3(camera.UpVector.Get()));
}
//-----------------------------------------------------------------------------
void Camera::SetNear(float near)
{
	mIsDirty = true;
	mNear = near;
}
//-----------------------------------------------------------------------------
void Camera::SetFar(float far)
{
	mIsDirty = true;
	mFar = far;
}
//-----------------------------------------------------------------------------
void Camera::SetFovy(float fovy)
{
	mIsDirty = true;
	mFovy = glm::radians(fovy);
}
//-----------------------------------------------------------------------------
void Camera::SetAspect(float asp)
{
	mIsDirty = true;
	mAsp = asp;
}
//-----------------------------------------------------------------------------
void Camera::SetPosition(const glm::vec3& position)
{
	mIsDirty = true;
	mPosition = position;
}
//-----------------------------------------------------------------------------
void Camera::SetCenter(const glm::vec3& center)
{
	mIsDirty = true;
	mCenter = center;
}
//-----------------------------------------------------------------------------
void Camera::SetUp(const glm::vec3& up)
{
	mIsDirty = true;
	mUp = up;
}
//-----------------------------------------------------------------------------
void Camera::ComputeTransforms() const
{
	mView = glm::lookAt(mPosition, mCenter, mUp);
	mPerspective = glm::perspective(mFovy, mAsp, mNear, mFar);
	mIsDirty = false;

}
//-----------------------------------------------------------------------------
const glm::mat4* Camera::GetPerspective() const
{
	if (mIsDirty) {
		ComputeTransforms();
	}

	return &mPerspective;
}
//-----------------------------------------------------------------------------
const glm::mat4* Camera::GetView() const
{
	if (mIsDirty) {
		ComputeTransforms();
	}

	return &mView;
}
//-----------------------------------------------------------------------------
void Camera::Translate(const glm::vec3& translation)
{
	mPosition += translation;
	mCenter += translation;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------
void Camera::RotateY(float angle)
{
	glm::vec3 viewDir = mCenter - mPosition;
	viewDir = glm::rotateY(viewDir, angle);
	mCenter = mPosition + viewDir;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------
void Camera::MoveU(float x)
{
	glm::vec3 u = glm::normalize(glm::cross(mUp, (mCenter - mPosition)));
	mPosition += x * u;
	mCenter += x * u;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------
void Camera::MoveN(float x)
{
	glm::vec3 n = glm::normalize((mCenter - mPosition));
	mPosition += x * n;
	mCenter += x * n;
	mIsDirty = true;
}
//-----------------------------------------------------------------------------