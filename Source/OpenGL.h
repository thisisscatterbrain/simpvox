#ifndef OPENGL_H
#define OPENGL_H

#include "Assert.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define GL_ASSERT_SUCCESS() ASSERT(GL_NO_ERROR == glGetError());

#endif