#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "Fbx.h"
#include "Entity.h"
#include <glm/mat4x4.hpp>

// Defines a transformation hierachy between meshes; used to transform meshes
// to world space.
class Transformation : public Entity<Transformation, 4096> {
public:
	Transformation(const FbxNode& node, const Transformation& parent);
	~Transformation();

	glm::mat4 global;

	// Transposed inverse of the global transformation matrix used to transform
	// normals to world space.
	glm::mat4 globalInvTrans;
	glm::mat4 local;

	// Parent in the tranformation hierachy; nullptr if the transform is root.
	const Transformation* parent;
};



#endif