#ifndef VOXELGRID_H
#define VOXELGRID_H

#include "GLTexture3D.h"
#include <glm/vec3.hpp>

// A cubic voxel grid representation in GL.
struct VoxelGrid {
	void Init(const glm::vec3& center, GLfloat extent, GLsizei cellCount, 
		GLenum internalFormat);
	void Deinit();

	GLTexture3D texture;	// Texture storing voxel data.
	glm::vec3 center;	// Center in world space coordinates.
	GLfloat extent;		// Extent in all dimensions.
	GLsizei cellCount;	// Number of cells in all dimensions
};

extern VoxelGrid gVoxelGrid;

#endif