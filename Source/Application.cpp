#include "Application.h"
#include "ApplicationSettings.h"
#include "OpenGL.h"
#include "Assert.h"
#include <vector>

Application Application::instance;

static GLFWwindow* gWindow = nullptr;
static std::vector<UpdateHandler*> gUpdateHandlers;

//-----------------------------------------------------------------------------
Application::Application()
{
}
//-----------------------------------------------------------------------------
Application::~Application()
{
}
//-----------------------------------------------------------------------------
void Application::Init()
{
	ApplicationSettings::instance.Load();
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	gWindow = glfwCreateWindow(
		ApplicationSettings::instance.GetWindowWidth(),
		ApplicationSettings::instance.GetWindowHeight(),
		ApplicationSettings::instance.GetWindowTitle().c_str(),
		NULL, NULL);
	glfwMakeContextCurrent(gWindow);

	glewExperimental = GL_TRUE;
	glewInit();
	glGetError();
	ASSERT(gWindow);
}
//-----------------------------------------------------------------------------
void Application::Deinit()
{
	glfwTerminate();
}
//-----------------------------------------------------------------------------
void Application::Run()
{
	double t = glfwGetTime();
	double dt;

	while (!glfwWindowShouldClose(gWindow)) {
		dt = glfwGetTime() - t;
		t = glfwGetTime();

		for (UpdateHandler* handler : gUpdateHandlers) {
			handler->OnUpdate(dt);
		}

		glfwSwapBuffers(gWindow);
		glfwPollEvents();
	}
}
//-----------------------------------------------------------------------------
void Application::AddUpdateHandler(UpdateHandler& handler)
{
	gUpdateHandlers.push_back(&handler);
}
//-----------------------------------------------------------------------------