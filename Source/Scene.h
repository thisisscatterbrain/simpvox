#ifndef SCENE_H
#define SCENE_H

#include "ClassDecls.h"
#include "UpdateHandler.h"

// Declares the scene component. The component responsible for setting up 
// entities that describe the scene.
class Scene : public UpdateHandler {
	DECL_SINGLETON(Scene)
public:

	// Initializes [Mesh], [Material], [PointLight] entities from a .fbx file; also
	// sets the camera accordingly.
	void Load(const char* filename);

	// Updates the global transformations of the scene based on the current
	// local transformations.
	void OnUpdate(float);
};

#endif