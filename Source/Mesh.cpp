#include "Mesh.h"
#include "Log.h"
#include <glm/glm.hpp>

//-----------------------------------------------------------------------------
static void ConvertMaterialLamb(Material* mat,
	const FbxSurfaceMaterial& fbxmat)
{
	FbxSurfaceLambert* m = (FbxSurfaceLambert*)(&fbxmat);

	mat->diffAtten = static_cast<float>(m->DiffuseFactor.Get());
	mat->diffRefl[0] = static_cast<float>(m->Diffuse.Get()[0]);
	mat->diffRefl[1] = static_cast<float>(m->Diffuse.Get()[1]);
	mat->diffRefl[2] = static_cast<float>(m->Diffuse.Get()[2]);

	mat->ambAtten = static_cast<float>(m->AmbientFactor.Get());
	mat->ambRefl[0] = static_cast<float>(m->Ambient.Get()[0]);
	mat->ambRefl[1] = static_cast<float>(m->Ambient.Get()[1]);
	mat->ambRefl[2] = static_cast<float>(m->Ambient.Get()[2]);
}
//-----------------------------------------------------------------------------
static void ConvertMaterialPhong(Material* mat, 
	const FbxSurfaceMaterial& fbxmat)
{
	FbxSurfacePhong* m = (FbxSurfacePhong*)(&fbxmat);

	// Phong material "is as" Lambert material ...
	ConvertMaterialLamb(mat, fbxmat);
	mat->specAtten = static_cast<float>(m->SpecularFactor.Get());
	mat->specRefl[0] = static_cast<float>(m->Specular.Get()[0]);
	mat->specRefl[1] = static_cast<float>(m->Specular.Get()[1]);
	mat->specRefl[2] = static_cast<float>(m->Specular.Get()[2]);
}
//-----------------------------------------------------------------------------
Material::Material(const FbxSurfaceMaterial& material)
{
	if (material.GetClassId().Is(FbxSurfacePhong::ClassId)) {
		ConvertMaterialPhong(this, material);
	} else if (material.GetClassId().Is(FbxSurfaceLambert::ClassId)) {
		ConvertMaterialLamb(this, material);
	} else {
		FAIL("Undefined material");
	}
}
//-----------------------------------------------------------------------------
Material::~Material()
{
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
static glm::vec3 GetNormal(int vertexId, int controlpointId,
	const FbxMesh& mesh)
{
	if (mesh.GetElementNormalCount() > 1)
		LOG("Mesh has more than one normal per vertex. Currently only the first normal is read, ignoring the others.");

	int id = 0;
	int idx = 0;
	const FbxGeometryElementNormal* normals = mesh.GetElementNormal(0);
	int count = normals->GetDirectArray().GetCount();

	// check normal mapping
	if (normals->GetMappingMode() == FbxGeometryElement::eByPolygonVertex) {
		id = vertexId;
	} else if (normals->GetMappingMode() == FbxGeometryElement::eByControlPoint){
		id = controlpointId;
	} else {
		FAIL("Unsupported mapping mode. Mapping mode is %d for mesh %s",
			normals->GetMappingMode(), mesh.GetNode()->GetName());
	}

	switch (normals->GetReferenceMode()) {
	case FbxGeometryElement::eDirect:
		return GlmVec3FromFbxVector4(normals->GetDirectArray().GetAt(id));
	case FbxGeometryElement::eIndexToDirect:
		idx = normals->GetIndexArray().GetAt(id);
		return GlmVec3FromFbxVector4(normals->GetDirectArray().GetAt(idx));
	default:
		FAIL("Reference mode not supported.");
	}

	return glm::vec3(0.0f, 0.0f, 0.0f);
}
//-----------------------------------------------------------------------------
static void InitializeMesh(Mesh* mesh, const FbxMesh& fbxMesh, 
	const FbxSurfaceMaterial& material)
{
	int nPolys = fbxMesh.GetPolygonCount();
	FbxVector4* controlpoints = fbxMesh.GetControlPoints();
	mesh->faceCount = 0;

	for (int i = 0; i < nPolys; i++) {

		// ignore polys with no material
		if (fbxMesh.GetElementMaterialCount() == 0) {
			LOG(false, "Ignoring polygon with no material.");
			continue;
		}

		// currently we support only one "material layer".
		if (fbxMesh.GetElementMaterialCount() > 1) {
			LOG("currently only one material layer supported...");
		}

		const FbxGeometryElementMaterial* matelem =
			fbxMesh.GetElementMaterial(0);
		FbxSurfaceMaterial* mat =
			fbxMesh.GetNode()->GetMaterial(
			matelem->GetIndexArray().GetAt(i));

		if (mat != (&material)) {
			continue;
		}

		for (int j = 0; j < fbxMesh.GetPolygonSize(i); j++) {

			// assure that we only get triangles.
			ASSERT(fbxMesh.GetPolygonSize(i) == 3);

			// set the vertices for the mesh
			int idx = fbxMesh.GetPolygonVertex(i, j);
			int vidx = fbxMesh.GetPolygonVertices()[fbxMesh.GetPolygonVertexIndex(i) + j];
			glm::vec3 pos;
			glm::vec3 norm;
			mesh->positions.push_back(GlmVec3FromFbxVector4(controlpoints[idx]));
			mesh->normals.push_back(GetNormal(vidx, idx, fbxMesh));
		}

		mesh->faceCount++;
	}

}
//-----------------------------------------------------------------------------
Mesh::Mesh(const FbxMesh& mesh, const FbxSurfaceMaterial& material,
	const Transformation& transformation)
{
	InitializeMesh(this, mesh, material);
	this->material = new Material(material);
	this->transformation = &transformation;
}
//-----------------------------------------------------------------------------
Mesh::~Mesh()
{
}
//-----------------------------------------------------------------------------
float Mesh::ComputeFaceArea(size_t faceIdx) const
{
	ASSERT(faceIdx < faceCount);
	glm::vec3 a = positions[3 * faceIdx + 1] - positions[3 * faceIdx];
	glm::vec3 b = positions[3 * faceIdx + 2] - positions[3 * faceIdx];
	return 0.5 * glm::length(glm::cross(a, b));
}
//-----------------------------------------------------------------------------
float Mesh::ComputeSurfaceArea() const 
{
	float a = ComputeFaceArea(0);
	for (size_t i = 0; i < faceCount; i++) {
		a += ComputeFaceArea(i);
	}
	return a;
}
//-----------------------------------------------------------------------------