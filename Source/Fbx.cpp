#include "Fbx.h"

//-----------------------------------------------------------------------------
glm::vec3 GlmVec3FromFbxDouble3(const FbxDouble3& fbxd3)
{
	glm::vec3 v3;
	v3[0] = static_cast<float>(fbxd3[0]);
	v3[1] = static_cast<float>(fbxd3[1]);
	v3[2] = static_cast<float>(fbxd3[2]);
	return v3;
}
//-----------------------------------------------------------------------------
bool IsRootNode(const FbxNode* node)
{
	return (node->GetScene()->GetRootNode() == node);
}
//-----------------------------------------------------------------------------
void SetLocalTransform(glm::mat4x4* m, const FbxNode& node)
{
	FbxNode* n = const_cast<FbxNode*>(&node);
	FbxAMatrix& fbxtrans = n->EvaluateLocalTransform();

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			(*m)[i][j] = static_cast<float>(fbxtrans.Get(i, j));
		}
	}
}
//-----------------------------------------------------------------------------
glm::vec3 GlmVec3FromFbxVector4(const FbxVector4& fbxv)
{
	glm::vec3 v;
	v[0] = static_cast<float>(fbxv[0]);
	v[1] = static_cast<float>(fbxv[1]);
	v[2] = static_cast<float>(fbxv[2]);
	return v;
}
//-----------------------------------------------------------------------------