#ifndef GLTEXTURE_H
#define GLTEXTURE_H

#include "GLResource.h"
#include "Common.h"

// Base class for a GL texture.
template <GLenum TARGET>
struct GLTexture : public GLResource {

	// Releases the texture.
	void Deinit();

	// Binds the texture to the current active texture unit.
	void Bind() const;

	// Binds the texture to a texture unit.
	void Bind(I8 unit) const;

	// Bind the first level image of the texture to an image unit.
	void BindImage(GLuint unit, GLenum access, GLenum format);

	// Gets the internal format for a texture image.
	GLint GetInternalImageFormat() const { return GetInternalImageFormat(0); }
	GLint GetInternalImageFormat(GLint level) const;

protected:
	static GLenum GetFormatFromInternalFormat(GLenum internalFormat);
	static GLenum GetTypeFromInternalFormat(GLenum internalFormat);
	static U8 GetChannelCount(GLenum internalFormat);
	void Generate();
};

#include "GLTexture.inl"

#endif