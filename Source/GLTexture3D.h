#ifndef GLTEXTURE3D_H
#define GLTEXTURE3D_H

#include "GLTexture.h"
#include "GLImage3D.h"

struct GLTexture3D : public GLTexture<GL_TEXTURE_3D> {
public:

	// The texture is given the following configuration:
	//	border = GL_REPEAT
	//	filter = GL_LINEAR
	void Init();
	void Init(GLint filter, GLint border);
	void SetImage(GLenum internalformat, const GLImage3D& image);
	GLImage3D GetImage(GLint level) const;

private:
	GLsizei GetImageWidth(GLint level) const;
	GLsizei GetImageHeight(GLint level) const;
	GLsizei GetImageDepth(GLint level) const;
};


#endif