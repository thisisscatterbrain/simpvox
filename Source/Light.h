#ifndef POINTLIGHT_H
#define POINTLIGHT_H

#include "Entity.h"
#include "Fbx.h"
#include <glm/vec3.hpp>

struct PointLight : public Entity<PointLight, 1024> {

	// Creates a point light from a fbx light. NOTE: Interprets light as a 
	// point light regardless of [light]'s type.
	PointLight(const FbxLight& light, const glm::vec3& position);
	~PointLight();

	glm::vec3 position;
	glm::vec3 color;
	float intensity;

	// attentuation coefficients for the point light.
	// constant, linear, quadratic
	glm::vec3 attenuation;
};

#endif