#ifndef ENTITY_H
#define ENTITY_H

#include <cstdio>
#include <cstdint>
#include <cassert>
#include "Assert.h"

template <class ENTITY, size_t SIZE>
struct Entity;

// Allocates memory for an entity; also deletes all entities (i.e. calls their 
// constructor) at the end of the process.
template<class ENTITY, size_t SIZE>
struct EntityPool {
	friend struct Entity<ENTITY, SIZE>;
	
	// Pool accessors
	ENTITY* begin();
	ENTITY* end();
	size_t GetCount();

	EntityPool();
	~EntityPool() { Reset(); }

private:
	void* Allocate();
	void Reset();

	static uint8_t buffer[SIZE];
	static uint8_t* alignedBuffer;
	static size_t count;
};

template<class ENTITY, size_t SIZE>
uint8_t EntityPool<ENTITY, SIZE>::buffer[SIZE];

template<class ENTITY, size_t SIZE>
uint8_t* EntityPool<ENTITY, SIZE>::alignedBuffer;

template<class ENTITY, size_t SIZE>
size_t EntityPool<ENTITY, SIZE>::count = 0;

// Base class for every entity. Every dynamically allocated entity is stored
// in contiguous memory, and globally accessable. [SIZE] denotes the size in
// bytes reserved for the entity pool. Entities cannot be deleted individually
// and are automatically destroyed at the end of the process.
template <class ENTITY, size_t SIZE>
struct Entity {
	static EntityPool<ENTITY, SIZE> pool;

	void* operator new(size_t size);
	void operator delete(void* ptr);
};

template <class ENTITY, size_t SIZE>
EntityPool<ENTITY, SIZE> Entity<ENTITY, SIZE>::pool;

#include "Entity.inl"

#endif