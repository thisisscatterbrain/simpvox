
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLTexture<TARGET>::Deinit()
{
	glDeleteTextures(1, &handle);
	handle = 0;
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLTexture<TARGET>::Bind() const
{
	glBindTexture(TARGET, handle);
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLTexture<TARGET>::Bind(I8 unit) const
{
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(TARGET, handle);
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLTexture<TARGET>::BindImage(GLuint unit, GLenum access, GLenum format)
{
	glBindImageTexture(0, handle, 0, GL_TRUE, 0, access, format);
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLTexture<TARGET>::Generate()
{
	glGenTextures(1, &handle);
	GL_ASSERT_SUCCESS()
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
GLint GLTexture<TARGET>::GetInternalImageFormat(GLint level) const
{
	GLint intFormat;
	Bind();
	glGetTexLevelParameteriv(TARGET, level, GL_TEXTURE_INTERNAL_FORMAT,
		&intFormat);
	GL_ASSERT_SUCCESS();
	return intFormat;
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
GLenum GLTexture<TARGET>::GetFormatFromInternalFormat(GLenum internalFormat)
{
	// Get the format for an internal format

	if (GL_RGB32F == internalFormat) {
		return GL_RGB;
	} else if (GL_RGB16F == internalFormat) {
		return GL_RGB;
	} else if (GL_RGBA32F == internalFormat) {
		return GL_RGBA;
	} else if (GL_R32F == internalFormat) {
		return GL_RED;
	} else if (GL_DEPTH_COMPONENT24 == internalFormat) {
		return GL_DEPTH_COMPONENT;
	} else {
		FAIL("Internal format not supported yet");
	}
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
U8 GLTexture<TARGET>::GetChannelCount(GLenum internalFormat)
{
	// Get the format for an internal format

	if (GL_RGB32F == internalFormat) {
		return 3;
	} else if (GL_RGB16F == internalFormat) {
		return 3;
	} else if (GL_RGBA32F == internalFormat) {
		return 4;
	} else if (GL_DEPTH_COMPONENT24) {
		return 1;
	} else {
		FAIL("Internal format not supported yet");
	}
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
GLenum GLTexture<TARGET>::GetTypeFromInternalFormat(
	GLenum internalFormat)
{
	if (GL_RGB32F == internalFormat) {
		return GL_FLOAT;
	} else if (GL_RGB16F == internalFormat) {
		return GL_FLOAT;
	} else if (GL_RGBA32F == internalFormat) {
		return GL_FLOAT;
	} else if (GL_DEPTH_COMPONENT24) {
		return GL_FLOAT;
	} else {
		FAIL("Internal format not supported yet");
	}
}
//-----------------------------------------------------------------------------
