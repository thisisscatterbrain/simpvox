#include "GLTexture2D.h"
#include <stb_image_write.h>

//-----------------------------------------------------------------------------
void GLTexture2D::Init()
{
	Init(GL_LINEAR, GL_REPEAT);
}
//-----------------------------------------------------------------------------
void GLTexture2D::Init(GLint filter, GLint border)
{
	Generate();
	Bind();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, border);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, border);
}
//-----------------------------------------------------------------------------
void GLTexture2D::AttachImage(GLenum internalformat, GLsizei width, 
	GLsizei height)
{
	Bind();
	glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, 
		GetFormatFromInternalFormat(internalformat), GL_UNSIGNED_BYTE, 0);
	GL_ASSERT_SUCCESS()
}
//-----------------------------------------------------------------------------
static void SwapPixel(U8* a, U8* b, int n)
{
	ASSERT(n < 5);
	unsigned char tmp[4];
	memcpy(tmp, a, n);
	memcpy(a, b, n);
	memcpy(b, tmp, n);
}
//-----------------------------------------------------------------------------
static void FlipY(U8* data, GLint w, GLint h, int n)
{
	// Flip the image vertically.

	GLint jmax = h / 2;

	for (GLint i = 0; i < w; i++) {
		for (GLint j = 0; j < jmax; j++) {
			GLint idxa = n * (j * w + i);
			GLint idxb = n * ((h - j - 1) * w + i);
			SwapPixel(data + idxa, data + idxb, n);
		}
	}
}
//-----------------------------------------------------------------------------
void GLTexture2D::SaveImage(const std::string& filename)
{
	Bind();
	ASSERT((filename.size() - 4) == filename.find(".bmp"));
	GLint width = GetImageWidth();
	GLint height = GetImageWidth();
	GLint intFormat = GetInternalImageFormat();
	GLint format = GetFormatFromInternalFormat(intFormat);
	U8 nChannels = GetChannelCount(intFormat);
	U8* pixels = new U8[width * height * nChannels];
	glGetTexImage(GL_TEXTURE_2D, 0, format, GL_UNSIGNED_BYTE, pixels);
	FlipY(pixels, width, height, nChannels);
	stbi_write_bmp(filename.c_str(), width, height, nChannels, pixels);
	delete[] pixels;
}
//-----------------------------------------------------------------------------
GLint GLTexture2D::GetImageWidth(GLint level) const
{
	GLint width;
	Bind();
	glGetTexLevelParameteriv(GL_TEXTURE_2D, level, GL_TEXTURE_WIDTH, &width);
	return width;
}
//-----------------------------------------------------------------------------
GLint GLTexture2D::GetImageHeight(GLint level) const
{
	GLint height;
	Bind();
	glGetTexLevelParameteriv(GL_TEXTURE_2D, level, GL_TEXTURE_WIDTH, &height);
	return height;
}
//-----------------------------------------------------------------------------
