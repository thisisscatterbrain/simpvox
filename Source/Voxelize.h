#ifndef VOXELIZE_H
#define VOXELIZE_H

// Voxelization operation. Takes the scene entities and updates the voxel grid.
class Voxelize {
public:
	static void Init();
	static void Deinit();
	static void Execute();
};

#endif
