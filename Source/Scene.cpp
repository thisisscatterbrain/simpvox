#include "Scene.h"
#include "Assert.h"
#include "Fbx.h"
#include "Transformation.h"
#include "Light.h"
#include "Camera.h"
#include "Mesh.h"
#include <map>
#include <glm/glm.hpp>

Scene Scene::instance;

//-----------------------------------------------------------------------------
Scene::Scene()
{
}
//-----------------------------------------------------------------------------
Scene::~Scene()
{
}
//-----------------------------------------------------------------------------
static bool LoadFile(FbxScene** scene, FbxManager** manager,
	const char* filename)
{
	*manager = FbxManager::Create();
	FbxIOSettings* ios = FbxIOSettings::Create(*manager, IOSROOT);
	(*manager)->SetIOSettings(ios);
	FbxImporter* importer = FbxImporter::Create(*manager, "");

	if (!importer->Initialize(filename, -1,
		(*manager)->GetIOSettings())) {
		(*manager)->Destroy();
		return false;
	}

	*scene = FbxScene::Create(*manager, "");
	importer->Import(*scene);
	importer->Destroy();

	// Triangulate all meshes in the scene.
	FbxGeometryConverter conv(*manager);
	conv.Triangulate(*scene, true);
	return true;
}
//-----------------------------------------------------------------------------
// Helper struct for initializing meshes.
struct MeshHeader {
	MeshHeader() : material(nullptr), faceCount(0) {}
	~MeshHeader() {}

	FbxSurfaceMaterial* material;
	size_t faceCount;
};
//-----------------------------------------------------------------------------
MeshHeader* GetMeshHeader(MeshHeader* headers, size_t headerCount,
	FbxSurfaceMaterial* material)
{
	// Get a mesh [header] for a fbx [material]; sets a the [material] if
	// there exists no [header] for [material] (as indicated by a null pointer)

	for (size_t i = 0; i < headerCount; i++) {
		if (!headers[i].material)
			headers[i].material = material;

		if (headers[i].material == material)
			return &headers[i];
	}

	return nullptr;
}
//-----------------------------------------------------------------------------
// Initializes the mesh header helper struct
static void InitMeshHeaders(MeshHeader* headers, size_t headerCount,
	const FbxMesh& mesh)
{
	int npolys = mesh.GetPolygonCount();
	size_t nfaces = 0;

	for (int i = 0; i < npolys; i++) {
		// assure that we only get triangles.
		ASSERT(mesh.GetPolygonSize(i) == 3);

		const FbxGeometryElementMaterial* matelem =
			mesh.GetElementMaterial(0);
		FbxSurfaceMaterial* mat = mesh.GetNode()->GetMaterial(
			matelem->GetIndexArray().GetAt(i));

		MeshHeader* header = GetMeshHeader(headers, headerCount, mat);
		header->faceCount++;
	}
}
//-----------------------------------------------------------------------------
static size_t CountMaterialsForFbxMesh(const FbxMesh& mesh)
{
	// Counts unique materials for a FbxMesh. 
	std::map<FbxSurfaceMaterial*, bool> matmeshmap;
	int npolys = mesh.GetPolygonCount();

	for (int i = 0; i < npolys; i++) {

		// assure that we only get triangles.
		assert(mesh.GetPolygonSize(i) == 3);

		const FbxGeometryElementMaterial* matelem =
			mesh.GetElementMaterial(0);
		FbxSurfaceMaterial* mat = mesh.GetNode()->GetMaterial(
			matelem->GetIndexArray().GetAt(i));
		matmeshmap[mat] = true;
	}

	return matmeshmap.size();
}
//-----------------------------------------------------------------------------
static void LoadMeshes(FbxNode& node, 
	const Transformation& transformation)
{
	FbxMesh* mesh = static_cast<FbxMesh*>(node.GetNodeAttribute());
	size_t nMaterials = CountMaterialsForFbxMesh(*mesh);
	MeshHeader* headers = new MeshHeader[nMaterials];
	InitMeshHeaders(headers, nMaterials, *mesh);

	for (size_t i = 0; i < nMaterials; i++) {
		new Mesh(*mesh, *headers[i].material, transformation);
	}

	delete[] headers;
}
//-----------------------------------------------------------------------------
static void LoadScene(FbxNode* parent, FbxNode* node, 
	const Transformation* trParent)
{
	Transformation* t = new Transformation(*node, *trParent);

	if (node->GetNodeAttribute()) {
		int type = node->GetNodeAttribute()->GetAttributeType();

		if (type == FbxNodeAttribute::eCamera) {
			if (!IsRootNode(parent)) {
				FAIL("Currently, the camera must be child of root.");
			}

			FbxCamera* camera = static_cast<FbxCamera*>(
				node->GetNodeAttribute());
			Camera::instance.Init(*camera);
		} else if (type == FbxNodeAttribute::eLight) {
			if (!IsRootNode(parent)) {
				FAIL("Currently, light nodes must be childs of root.");
			}

			FbxLight* light = static_cast<FbxLight*>(node->GetNodeAttribute());

			if (light->LightType.Get() == FbxLight::ePoint) {
				// Infer the position of the light.
				glm::mat4 trans;
				SetLocalTransform(&trans, *node);
				glm::vec3 pos = glm::vec3(trans * glm::vec4(0.0, 0.0, 0.0, 1.0));
				new PointLight(*light, pos);
			}
		} else if (type == FbxNodeAttribute::eMesh) {
			LoadMeshes(*node, *t);
		}

	}

	int nChilds = node->GetChildCount();

	for (int i = 0; i < nChilds; i++) {
		LoadScene(node, node->GetChild(i), t);
	}
}
//-----------------------------------------------------------------------------
void Scene::Load(const char* filename)
{
	FbxScene* scene;
	FbxManager* manager;
	ASSERT(LoadFile(&scene, &manager, filename));
	LoadScene(nullptr, scene->GetRootNode(), nullptr);
	manager->Destroy();
}
//-----------------------------------------------------------------------------
void Scene::OnUpdate(float dt)
{
	ASSERT(Transformation::pool.GetCount());

	// NOTE: for this to work transformation need to be read in depth first.

	Transformation* trans = Transformation::pool.begin();
	trans->global = trans->local;
	trans++;

	for (; trans != Transformation::pool.end(); trans++) {
		trans->global = trans->parent->global * trans->local;
		trans->globalInvTrans = glm::transpose(glm::inverse(trans->global));
	}

}
//-----------------------------------------------------------------------------
