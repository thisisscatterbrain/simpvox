#ifndef GLIMAGE3D_H
#define GLIMAGE3D_H

#include "GLImage.h"
#include "ClassDecls.h"

// A container for 3D image data.
struct GLImage3D {
	GLImage3D();
	GLImage3D(GLImage3D&& other);
	GLImage3D(GLuint width, GLuint height, GLuint depth, GLenum format,
		GLenum type, GLvoid* data, bool copy = true);
	GLImage3D(GLuint width, GLuint height, GLuint depth, GLenum format, 
		GLenum type, bool allocData = true);
	GLImage3D& operator=(GLImage3D&& other);
	~GLImage3D();

	// (Unsafe) Data access.
	template <typename T>
	T GetAs(U32 index) const;
	template <typename T>
	T* GetDataAs(U32 index) const;

	GLsizei width;
	GLsizei height;
	GLsizei depth;
	GLenum format;
	GLenum type;
	GLvoid* data;
};

#include "GLImage3D.inl"

#endif