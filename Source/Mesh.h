#ifndef MESH_H
#define MESH_H

#include <vector>
#include "Fbx.h"
#include "Entity.h"
#include "Transformation.h"
#include <glm/vec3.hpp>

struct Material : public Entity<Material, 1024> {
	Material(const FbxSurfaceMaterial& material);
	~Material();

	// Basic material properties
	float specAtten;
	float diffAtten;
	float ambAtten;
	glm::vec3 ambRefl;
	glm::vec3 diffRefl;
	glm::vec3 specRefl;
};

struct Mesh : public Entity<Mesh, 1024> {
	// Initializes the mesh with an fbx mesh and material; also creates the
	// [Material] for the mesh.
	Mesh(const FbxMesh& mesh, const FbxSurfaceMaterial& material, 
		const Transformation& transformation);
	~Mesh();

	// Computes the surface area of the mesh.
	float ComputeSurfaceArea() const;

	// Computes the area of a certain face of the mesh. NOTE: [faceIdx] is 
	// assumed to be within 0 and [faceCount] - 1.
	float ComputeFaceArea(size_t faceIdx) const;

	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	size_t faceCount;
	const Material* material;
	const Transformation* transformation;
};

#endif