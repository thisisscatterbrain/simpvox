#include "FullscreenQuad.h"

FullscreenQuad gFullscreenQuad;

//-----------------------------------------------------------------------------
void FullscreenQuad::Init()
{
	static GLfloat quad[] = {
		-1.0, -1.0,
		+1.0, -1.0,
		+1.0, +1.0,

		-1.0, -1.0,
		+1.0, +1.0,
		-1.0, +1.0
	};

	mAttribBuf.Init();
	mAttribBuf.SetBufferData(sizeof(quad), quad, GL_STATIC_DRAW);
	mVertArr.Init();
	mVertArr.SetVertexAttribute(mAttribBuf);
	GL_ASSERT_SUCCESS()
}
//-----------------------------------------------------------------------------
void FullscreenQuad::Deinit()
{
	mAttribBuf.Deinit();
	mVertArr.Deinit();
}
//-----------------------------------------------------------------------------
void FullscreenQuad::Draw(GLenum mode) const
{
	mVertArr.Draw(mode, 0 , 6);
}
//-----------------------------------------------------------------------------
