#ifndef APPLICATION_H
#define APPLICATION_H

#include "ClassDecls.h"
#include "UpdateHandler.h"

class Application {
	DECL_SINGLETON(Application)
public:
	// Initializes the application with default settings.
	void Init();
	void Deinit();

	void AddUpdateHandler(UpdateHandler& updateHandler);

	// Runs the main loop of the application.
	void Run();
};

#endif