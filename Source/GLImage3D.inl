//-----------------------------------------------------------------------------
template <typename T>
T GLImage3D::GetAs(U32 index) const
{
	return ((static_cast<T*>(data))[index]);
}
//-----------------------------------------------------------------------------
template <typename T>
T* GLImage3D::GetDataAs(U32 index) const
{
	return static_cast<T*>(data);
}
//-----------------------------------------------------------------------------