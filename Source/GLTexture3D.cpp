#include "GLTexture3D.h"
#include <memory>

//-----------------------------------------------------------------------------
void GLTexture3D::Init()
{
	Init(GL_NEAREST, GL_REPEAT);
}
//-----------------------------------------------------------------------------
void GLTexture3D::Init(GLint filter, GLint border)
{
	GL_ASSERT_SUCCESS();
	Generate();
	Bind();
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, filter);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, border);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, border);
	GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
void GLTexture3D::SetImage(GLenum internalformat, const GLImage3D& image)
{
	Bind();
	GL_ASSERT_SUCCESS();
	glTexImage3D(GL_TEXTURE_3D, 0, internalformat, image.width, 
		image.height, image.depth, 0, image.format, image.type, 
		image.data);
	GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
GLImage3D GLTexture3D::GetImage(GLint level) const
{
	Bind();
	GLenum intForm = GetInternalImageFormat(level);
	GLenum format = GetFormatFromInternalFormat(intForm);
	GLenum type = GetTypeFromInternalFormat(intForm);
	GLsizei width = GetImageWidth(level);
	GLsizei height = GetImageHeight(level);
	GLsizei depth = GetImageDepth(level);
	GLvoid* data = malloc(width * height * depth * 
		GLImage::GetPixelSize(format, type));
	glGetTexImage(GL_TEXTURE_3D, level, format, type, data);
	return GLImage3D(width, height, depth, format, type, data, false);
}
//-----------------------------------------------------------------------------
GLsizei GLTexture3D::GetImageWidth(GLint level) const
{
	GLint width;
	Bind();
	glGetTexLevelParameteriv(GL_TEXTURE_3D, level, GL_TEXTURE_WIDTH, 
		&width);
	return width;
}
//-----------------------------------------------------------------------------
GLsizei GLTexture3D::GetImageHeight(GLint level) const
{
	GLint height;
	Bind();
	glGetTexLevelParameteriv(GL_TEXTURE_3D, level, GL_TEXTURE_HEIGHT,
		&height);
	return height;
}
//-----------------------------------------------------------------------------
GLsizei GLTexture3D::GetImageDepth(GLint level) const
{
	GLint depth;
	Bind();
	glGetTexLevelParameteriv(GL_TEXTURE_3D, level, GL_TEXTURE_DEPTH,
		&depth);
	return depth;
}
//-----------------------------------------------------------------------------