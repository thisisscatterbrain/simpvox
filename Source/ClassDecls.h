#ifndef CLASSDECLS_H
#define CLASSDECLS_H

#define DECL_SINGLETON(NAME)		\
public:								\
	static NAME instance;			\
private:							\
	NAME();							\
	NAME(const NAME&);				\
	NAME& operator=(const NAME&);	\
	virtual ~NAME();


#define DECL_NOT_COPYABLE(NAME)					\
private:									\
	NAME(const NAME& orig) {}				\
	NAME& operator=(const NAME& orig) {}	

// Declares read only member
#define RO_MEMBER(T, X)								\
	private:										\
		T m##X;										\
	public:											\
		const T& Get##X () const { return m##X; }


#endif