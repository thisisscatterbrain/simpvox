#include "DeviceSurface.h"

//-----------------------------------------------------------------------------
DeviceSurface::DeviceSurface(const Mesh& mesh)
	:
	vertexCount(mesh.faceCount * 3)
{
	positionBuffer.Init();
	positionBuffer.SetBufferData(mesh.positions.size() * sizeof(glm::vec3), 
		mesh.positions.data(), GL_STATIC_DRAW);
	vertexArray.Init();
	vertexArray.SetVertexAttribute(positionBuffer);
}
//-----------------------------------------------------------------------------
DeviceSurface::~DeviceSurface()
{
	positionBuffer.Deinit();
	vertexArray.Deinit();
}
//-----------------------------------------------------------------------------
void DeviceSurface::Draw(GLenum mode)
{
	vertexArray.Draw(mode, 0, vertexCount);
}
//-----------------------------------------------------------------------------
