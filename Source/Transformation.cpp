#include "Transformation.h"

//-----------------------------------------------------------------------------
Transformation::Transformation(const FbxNode& node, 
	const Transformation& parent)
	:
	parent(&parent)
{
	SetLocalTransform(&local, node);
}
//-----------------------------------------------------------------------------
Transformation::~Transformation()
{
}
//-----------------------------------------------------------------------------