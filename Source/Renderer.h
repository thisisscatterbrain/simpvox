#ifndef RENDERER_H
#define RENDERER_H

#include "UpdateHandler.h"
#include "ClassDecls.h"

class Renderer : public UpdateHandler {
	DECL_SINGLETON(Renderer)
public:
	void Init();
	void Deinit();
	void OnUpdate(float dt);
};

#endif