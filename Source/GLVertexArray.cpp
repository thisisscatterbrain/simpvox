#include "GLVertexArray.h"

//-----------------------------------------------------------------------------
void GLVertexArray::Init()
{
	glGenVertexArrays(1, &handle);
}
//-----------------------------------------------------------------------------
void GLVertexArray::Deinit()
{
	glDeleteVertexArrays(1, &handle);
	handle = 0;
}
//-----------------------------------------------------------------------------
void GLVertexArray::Draw(GLenum mode, GLint first, GLsizei count) const
{
	glBindVertexArray(handle);
	glDrawArrays(mode, first, count);
}
//-----------------------------------------------------------------------------