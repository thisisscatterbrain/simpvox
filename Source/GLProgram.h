#ifndef PROGRAM_H
#define PROGRAM_H

#include "GLResource.h"
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <string>

struct GLProgram : public GLResource {

	// Initializes a GL Program from shader files.
	void InitFromFiles(const std::vector<std::string>& files);
	void Deinit();

	bool IsBound() const;
	void Bind() const;

	// Uniform setters
	void SetUniform(const std::string& name, GLint i);
	void SetUniform(const std::string& name, GLfloat f);
	void SetUniform(const std::string& name, const glm::vec3& vec3);
	void SetUniform(const std::string& name, const glm::mat4& mat4);

protected:
	GLint GetUniformLocation(const std::string& name);
};

#endif