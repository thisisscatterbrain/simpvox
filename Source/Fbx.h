#ifndef FBX_H
#define FBX_H

#include <fbxsdk.h>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

glm::vec3 GlmVec3FromFbxDouble3(const FbxDouble3& fbxd3);
glm::vec3 GlmVec3FromFbxVector4(const FbxVector4& fbxv);
bool IsRootNode(const FbxNode* node);
void SetLocalTransform(glm::mat4x4* m, const FbxNode& node);

#endif