#include "OpenGLWrapper.h"
#include "Assert.h"
#include "Log.h"
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <stb_image_write.h>
#include <glm/gtc/type_ptr.hpp>

//-----------------------------------------------------------------------------
static char* ReadFile(const char* filename)
{
	// read contents of a file and return a zero terminated string or 
	// NULL if the file could not be opened.

	FILE* f = NULL;
	size_t numChars = 1; /* # of chars in the file */
	char* contents = NULL;
	char ch = 0;
	size_t count = 0;

	f = fopen(filename, "r");

	if (!f)
	{
		return NULL;
	}

	/* count # of chars in the file */
	while (EOF != fgetc(f))
	{
		numChars++;
	}

	contents = (char*)malloc(numChars);

	if (!contents)
	{
		fclose(f);
		return NULL;
	}

	/* rewind and load the file's contents */
	rewind(f);

	while (1)
	{
		ch = fgetc(f);

		/* break if end of file is reached, don't forget to finish the string */
		if (ch == EOF)
		{
			contents[count] = '\0';
			break;
		}

		contents[count] = ch;
		count++;
	}

	/* clean up and return the contents */
	fclose(f);

	return contents;
}
//-----------------------------------------------------------------------------
static int AttachShaderWithSource(GLuint program, GLenum type,
	const char* source)
{
	GLuint shader = 0;
	GLint status;
	GLint infoLogLength; /* length of the info log */
	GLchar* infoLog; 	 /* the info log */

	shader = glCreateShader(type);

	if (!shader)
	{
		FAIL("Failed to create shader");
		return 0;
	}

	glShaderSource(shader, 1, &source, NULL);
	ASSERT(GL_NO_ERROR == glGetError())
	glCompileShader(shader);
	ASSERT(GL_NO_ERROR == glGetError())

	// check if the shader compiled correctly
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	ASSERT(GL_NO_ERROR == glGetError())

	// complain if something went wrong 
	if (status == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		infoLog = (GLchar*)malloc(infoLogLength + 1);
		glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);
		FAIL(infoLog);
		free(infoLog);
		glDeleteShader(shader);
		return 0;
	}

	glAttachShader(program, shader);
	ASSERT(GL_NO_ERROR == glGetError())
	return 1;
}
//-----------------------------------------------------------------------------
// Attach shader from file
static int AttachShaderWithFile(GLuint program, GLenum type,
	const char* file)
{
	char* contents = NULL; /* contents of the file */
	int succ = 0;

	contents = ReadFile(file);

	if (!contents) {
		FAIL("Could not load shader file");
		return 0;
	}

	succ = AttachShaderWithSource(program, type, contents);
	ASSERT(NULL != contents);

	/* clean up */
	free(contents);
	return succ;
}
//-----------------------------------------------------------------------------
static GLenum GetTypeFromFilename(const char* filename)
{
	if (strstr(filename, ".vs")) {
		return GL_VERTEX_SHADER;
	} else if (strstr(filename, ".fs")) {
		return GL_FRAGMENT_SHADER;
	} else if (strstr(filename, ".gs")) {
		return GL_GEOMETRY_SHADER;
	}

	FAIL("Unsupported shader type");
}
//-----------------------------------------------------------------------------
static int LinkProgram(GLuint program)
{
	GLint status = 0;
	GLint infoLogLength; /* length of the info log */
	GLchar* infoLog; 	 /* the info log */
	GLint numShaders; 	 /* # of shader attached to the program */
	GLuint* shaders;     /* shaders attached to the program */
	int i = 0;

	// link the program
	glLinkProgram(program);

	// check if everything went well
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	// complain if s.th went wrong
	if (status == GL_FALSE)
	{
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
		infoLog = (char*)malloc(infoLogLength + 1);
		glGetProgramInfoLog(program, infoLogLength, NULL, infoLog);
		FAIL(infoLog);
		free(infoLog);
		return 0;
	}

	// detach and delete the shaders from the program
	glGetProgramiv(program, GL_ATTACHED_SHADERS, &numShaders);
	shaders = (GLuint*)malloc(sizeof(GLuint)*numShaders);
	glGetAttachedShaders(program, numShaders, NULL, shaders);

	for (i = 0; i < numShaders; i++) {
		glDetachShader(program, shaders[i]);
		glDeleteShader(shaders[i]);
	}

	return 1;
}
//-----------------------------------------------------------------------------
GLuint GLCreateProgram(const char** filenames)
{
	GLuint program = glCreateProgram();
	const char** fnames = filenames;

	while (*fnames) {
		AttachShaderWithFile(program, GetTypeFromFilename(*fnames), *fnames);
		fnames++;
	}
	
	LinkProgram(program);
	return program;
}
//-----------------------------------------------------------------------------
void GLUniform1I(GLuint program, const GLchar* name, GLint i)
{
	GLint loc = glGetUniformLocation(program, name);

	if (loc < 0) {
		LOG("Variable %s not found", name)
	}

	glUniform1i(loc, i);
}
//-----------------------------------------------------------------------------
void GLUniform1F(GLuint program, const GLchar* name, float f)
{
	GLint loc = glGetUniformLocation(program, name);

	if (loc < 0) {
		LOG("Variable %s not found", name)
	}

	glUniform1f(loc, f);
}
//-----------------------------------------------------------------------------
void GLUniform2F(GLuint program, const GLchar* name, const glm::vec2& v2)
{
	GLint loc = glGetUniformLocation(program, name);

	if (loc < 0) {
		LOG("Variable %s not found", name)
	}

	glUniform2f(loc, v2[0], v2[1]);
}
//-----------------------------------------------------------------------------
void GLUniform3F(GLuint program, const GLchar* name, const glm::vec3& v3)
{
	GLint loc = glGetUniformLocation(program, name);

	if (loc < 0) {
		LOG("Variable %s not found", name)
	}

	glUniform3f(loc, v3[0], v3[1], v3[2]);
}
//-----------------------------------------------------------------------------
void GLUniformMat4F(GLuint program, const GLchar* name, const glm::mat4& mat)
{
	GLint loc = glGetUniformLocation(program, name);

	if (loc < 0) {
		LOG("Variable %s not found", name)
	}

	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(mat));
}
//-----------------------------------------------------------------------------
static GLenum GetFormatFromInternalFormat(GLenum internalFormat)
{
	// Get the format for an internal format

	if (GL_RGB32F == internalFormat) {
		return GL_RGB;
	} else if (GL_RGB16F == internalFormat) {
		return GL_RGB;
	} else if (GL_RGBA32F == internalFormat) {
		return GL_RGBA;
	} else if (GL_DEPTH_COMPONENT24) {
		return GL_DEPTH_COMPONENT;
	} else {
		FAIL("Internal format not supported yet");
	}
}
//-----------------------------------------------------------------------------
static int GetChannelCount(GLenum internalFormat)
{
	// Get the format for an internal format

	if (GL_RGB32F == internalFormat) {
		return 3;
	} else if (GL_RGB16F == internalFormat) {
		return 3;
	} else if (GL_RGBA32F == internalFormat) {
		return 4;
	} else if (GL_DEPTH_COMPONENT24) {
		return 1;
	} else {
		FAIL("Internal format not supported yet");
	}
}
//-----------------------------------------------------------------------------
static void GetTexture2DInfo(GLint* w, GLint* h, GLint* n, 
	GLenum* internalFormat, GLenum* format, GLuint texture, GLuint target, 
	bool isCubeMap)
{
	// Get information needed to retrieve pixel data from a 2d GL texture.

	GLint intFormat;

	if (isCubeMap) {
		glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
	} else {
		glBindTexture(target, texture);
	}
	
	glGetTexLevelParameteriv(target, 0, GL_TEXTURE_WIDTH, w);
	glGetTexLevelParameteriv(target, 0, GL_TEXTURE_HEIGHT, h);
	glGetTexLevelParameteriv(target, 0, GL_TEXTURE_INTERNAL_FORMAT,
		&intFormat);
	ASSERT(GL_NO_ERROR == glGetError());
	*internalFormat = intFormat;
	*format = GetFormatFromInternalFormat(*internalFormat);
	*n = GetChannelCount(*internalFormat);
}
//-----------------------------------------------------------------------------
GLuint GLCreateTexture2D(GLsizei width, GLsizei height, GLenum internalFormat)
{
	// translate internal format to correct format parameter
	GLenum format = GetFormatFromInternalFormat(internalFormat);
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, 
		GL_UNSIGNED_BYTE, nullptr);
	ASSERT(GL_NO_ERROR == glGetError())
	return tex;
}
//-----------------------------------------------------------------------------
GLuint GLCreateTexture2D(GLsizei width, GLsizei height, GLenum internalFormat,
	GLenum boundary, GLenum filter)
{
	// translate internal format to correct format parameter
	GLenum format = GetFormatFromInternalFormat(internalFormat);
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, boundary);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, boundary);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format,
		GL_UNSIGNED_BYTE, nullptr);
	ASSERT(GL_NO_ERROR == glGetError());
	return tex;
}
//-----------------------------------------------------------------------------
GLuint GLCreateCubeMap(GLsizei width, GLsizei height, GLenum internalFormat)
{
	// translate internal format to correct format parameter
	GLenum format = GetFormatFromInternalFormat(internalFormat);
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	for (int i = 0; i < 6; i++) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
			internalFormat, width, height, 0, format, 
			GL_UNSIGNED_BYTE, nullptr);
	}

	ASSERT(GL_NO_ERROR == glGetError());
	return tex;
}
//-----------------------------------------------------------------------------
static void GetTexture2DData(uint8_t** data, GLint* w, GLint* h, int* n,
	GLuint texture, GLuint target, bool isCubeMap)
{
	// Get the contents of a 2d texture. Allocates memory for storing
	// the texture's pixel data, do not forget to free it in the caller!

	GLenum internalFormat = 0;
	GLenum format = 0;
	GetTexture2DInfo(w, h, n, &internalFormat, &format, texture, target,
		isCubeMap);
	*data = new uint8_t[(*w) * (*h) * (*n)];
	glGetTexImage(target, 0, format, GL_UNSIGNED_BYTE, *data);
	ASSERT(GL_NO_ERROR == glGetError());
}
//-----------------------------------------------------------------------------
static void SwapPixel(unsigned char* a, unsigned char* b, int n)
{
	ASSERT(n < 5)
	unsigned char tmp[4];
	memcpy(tmp, a, n);
	memcpy(a, b, n);
	memcpy(b, tmp, n);
}
//-----------------------------------------------------------------------------
static void FlipY(unsigned char* data, GLint w, GLint h, int n)
{
	// Flip the image vertically.

	GLint jmax = h / 2;

	for (GLint i = 0; i < w; i++) {
		for (GLint j = 0; j < jmax; j++) {
			GLint idxa = n * (j * w + i);
			GLint idxb = n * ((h - j - 1) * w + i);
			SwapPixel(data + idxa, data + idxb, n);
		}
	}
}
//-----------------------------------------------------------------------------
void GLSaveTexture2D(GLuint texture, const char* filename)
{
	uint8_t* data;
	GLint w, h, n;
	GetTexture2DData(&data, &w, &h, &n, texture, GL_TEXTURE_2D, false);
	FlipY(data, w, h, n);
	stbi_write_bmp(filename, w, h, n, data);
	delete[] data;
}
//-----------------------------------------------------------------------------
void GLSaveCubeMap2D(GLuint cubeMap, const char* filename)
{
	uint8_t* images[6];
	GLint w, h, n;

	// Get image data for each face
	for (int face = 0; face < 6; face++) {
		GetTexture2DData(images + face, &w, &h, &n, cubeMap,
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, true);
	}

	// Allocate buffer for storing the cube map data.
	GLint cmWidth = 6 * w;	// [w] (and [h]) are assumed to be the same 
				// for each face ...

	uint8_t* cmBuf = new uint8_t[cmWidth * h * n];
	
	// copy data of the individual face to the cube map buffer

	for (int face = 0; face < 6; face++) {
		for (GLint i = 0; i < w; i++) {
			for (GLint j = 0; j < h; j++) {
				GLint idxCM = (j * cmWidth + face * w + i) * n;
				GLint idx = (j * w + i) * n;
				memcpy(cmBuf + idxCM, images[face] + idx, n);
			}
		}
	}

	// Save cm to file.
	stbi_write_bmp(filename, cmWidth, h, n, cmBuf);

	// clean up
	for (int face = 0; face < 6; face++) {
		delete[] images[face];
	}
	
	delete[] cmBuf;
}
//-----------------------------------------------------------------------------
