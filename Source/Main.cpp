#include "Application.h"
#include "Renderer.h"
#include "GLTexture2D.h"
#include "GLVertexArray.h"
#include "Scene.h"
#include <cstdlib>
#include <string>
#include <vector>

void Pause()
{
	std::system("pause");
}

int main(int argc, char* argv[])
{
	atexit(Pause);
	Application::instance.Init();
	Scene::instance.Load("../Resources/Scenes/CornellBox.fbx");
	Renderer::instance.Init();
	Application::instance.AddUpdateHandler(Renderer::instance);
	Application::instance.Run();
	Renderer::instance.Deinit();
	Application::instance.Deinit();
	return 0;
}