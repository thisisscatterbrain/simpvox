//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLBuffer<TARGET>::Init()
{
	glGenBuffers(1, &handle);
	GL_ASSERT_SUCCESS()
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLBuffer<TARGET>::SetBufferData(GLsizei size, const GLvoid* data, 
	GLenum usage)
{
	Bind();
	glBufferData(TARGET, size, data, usage);
	GL_ASSERT_SUCCESS()
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLBuffer<TARGET>::Deinit()
{
	glDeleteBuffers(1, &handle);
	handle = 0;
}
//-----------------------------------------------------------------------------
template <GLenum TARGET>
void GLBuffer<TARGET>::Bind() const
{
	glBindBuffer(TARGET, handle);
}
//-----------------------------------------------------------------------------
