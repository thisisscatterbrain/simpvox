#ifndef COMMON_H
#define COMMON_H

#include <cstdint>

// Primitive types.
typedef float		F32;
typedef double		F64;
typedef int8_t		I8;
typedef uint8_t		U8;
typedef int16_t		I16;
typedef uint16_t	U16;
typedef int32_t		I32;
typedef uint32_t	U32;
typedef int64_t		I64;
typedef uint64_t	U64;

// Turns x to a string.
#define TO_STRING(x) #x

#endif