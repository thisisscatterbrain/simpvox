#ifndef FullscreenQuad_H
#define FullscreenQuad_H

#include "GLVertexArray.h"
#include "ClassDecls.h"

class FullscreenQuad {
public:
	void Init();
	void Deinit();
	void Draw(GLenum mode) const;

private:
	GLVertexArray mVertArr;
	GLAttribBufferPos2f mAttribBuf;
};

extern FullscreenQuad gFullscreenQuad;

#endif