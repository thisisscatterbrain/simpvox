#include "GLImage3D.h"
#include <memory>

//-----------------------------------------------------------------------------
GLImage3D::GLImage3D()
	:
	width(0),
	height(0),
	depth(0),
	format(GL_RGB),
	type(GL_UNSIGNED_BYTE),
	data(nullptr)
{
}
//-----------------------------------------------------------------------------
GLImage3D::GLImage3D(GLImage3D&& other)
	//:
	//width(other.width),
	//height(other.height),
	//depth(other.depth),
	//format(other.format),
	//type(other.type),
	//data(other.data)
{
	//other.data = nullptr;
	*this = other;
}
//-----------------------------------------------------------------------------
GLImage3D::GLImage3D(GLuint width, GLuint height, GLuint depth, GLenum format,
	GLenum type, bool allocData)
	:
	width(width),
	height(height),
	depth(depth),
	format(format),
	type(type),
	data(nullptr)
{
	if (allocData) {
		U32 size = GLImage::GetPixelSize(format, type) * width * 
			height * depth;
		data = malloc(size);
		memset(data, 0, size);
	}
}
//-----------------------------------------------------------------------------
GLImage3D::GLImage3D(GLuint width, GLuint height, GLuint depth, GLenum format,
	GLenum type, GLvoid* data, bool copy)
	:
	width(width),
	height(height),
	depth(depth),
	format(format),
	type(type)
{
	if (copy) {
		U32 size = GLImage::GetPixelSize(format, type) * width *
			height * depth;
		data = malloc(size);
		memcpy(this->data, data, size);
	} else {
		this->data = data;
	}
}
//-----------------------------------------------------------------------------
GLImage3D& GLImage3D::operator=(GLImage3D&& other)
{
	if (this == &other) {
		return *this;
	}

	width = other.width;
	height = other.height;
	depth = other.depth;
	format = other.format;
	type = other.type;
	data = other.data;
	other.data = nullptr;
	return *this;
}
//-----------------------------------------------------------------------------
GLImage3D::~GLImage3D()
{
	if (data) {
		free(data);
	}
}
//-----------------------------------------------------------------------------