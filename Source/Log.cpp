#include "Log.h"

static FILE* gFile = stderr;

void SetFile(FILE* file)
{
	gFile = file;
}

void Reportf(const char* filename, int line,
	const char* format, ...)
{
	printf("%s (%d) :\n", filename, line);
	va_list args;
	va_start(args, format);
	vfprintf(gFile, format, args);
	va_end(args);
	printf("\n");
}