#include "Light.h"
#include "Assert.h"

//-----------------------------------------------------------------------------
PointLight::PointLight(const FbxLight& light, const glm::vec3& position)
	:
	position(position),
	attenuation(1.0, 0.0, 0.0)
{
	color = GlmVec3FromFbxDouble3(light.Color.Get());
	intensity = light.Intensity.Get() / 100.0f;
}
//-----------------------------------------------------------------------------
PointLight::~PointLight()
{
}
//-----------------------------------------------------------------------------
