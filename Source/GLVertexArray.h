#ifndef GLVERTEXARRAY_H
#define GLVERTEXARRAY_H

#include "GLResource.h"
#include "GLBuffer.h"

struct GLVertexArray : public GLResource {
	void Init();
	void Deinit();

	template <GLuint INDEX, GLint DIM, GLenum TYPE>
	void SetVertexAttribute(const GLAttribBuffer<INDEX, DIM, TYPE>& buffer);
	void Draw(GLenum mode, GLint first, GLsizei count) const;
};

#include "GLVertexArray.inl"

#endif