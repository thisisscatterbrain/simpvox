#ifndef OPENGLWRAPPER_H
#define OPENGLWRAPPER_H

#include <GL/glew.h>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>

// Creates a program from shader files. [filenames] is a zero terminated array
// that contains the filenames for the shaders. Shaders files are identified
// by their postfixes: .vs (vertex shader), .fs (fragment shader), 
// .gs (geometry shader).
GLuint GLCreateProgram(const char** filenames);

// Uniform setters; NOTE: program should be bound before setting uniforms
void GLUniform1I(GLuint program, const GLchar* name, GLint i);
void GLUniform1F(GLuint program, const GLchar* name, float f);
void GLUniform2F(GLuint program, const GLchar* name, const glm::vec2& v2);
void GLUniform3F(GLuint program, const GLchar* name, const glm::vec3& v3);
void GLUniformMat4F(GLuint program, const GLchar* name, const glm::mat4& mat);

// Creates and configures a 2D texture.
GLuint GLCreateTexture2D(GLsizei width, GLsizei height, GLenum internalFormat);

GLuint GLCreateTexture2D(GLsizei width, GLsizei height, GLenum internalFormat,
	GLenum boundary, GLenum filter);

// Creates and configures a cube map.
GLuint GLCreateCubeMap(GLsizei width, GLsizei height, GLenum internalFormat);

// Saves the image of a [texture] to a file.
void GLSaveTexture2D(GLuint texture, const char* filename);

// Saves a cube map that use [GL_TEXTURE_2D] textures as image buffers.
void GLSaveCubeMap2D(GLuint cubeMap, const char* filename);

#endif