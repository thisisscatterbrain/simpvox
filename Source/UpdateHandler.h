#ifndef UPDATEHANDLER_H
#define UPDATEHANDLER_H

// Interface for handling update events send by the [Application] subsystem. 
class UpdateHandler {
public:
	UpdateHandler();
	virtual ~UpdateHandler();
	virtual void OnUpdate(float dt) = 0;
};

#endif