#ifndef GLTEXTURE2D_H
#define GLTEXTURE2D_H

#include "GLResource.h"
#include "GLTexture.h"
#include <string>

struct GLTexture2D : public GLTexture<GL_TEXTURE_2D> {

	// Creates and 2D texture. The texture is given the following default 
	// configuration:
	//	filter = GL_LINEAR
	//	border = GL_REPEAT
	void Init();

	// Initializes the texture and configures it according to [filter] and [border].
	void Init(GLint filter, GLint border);

	// Attaches an empty image to the texture at level 0 with border 0.
	void AttachImage(GLenum internalformat, GLsizei width, GLsizei height);

	// Saves the image at level 0 to a .bmp file.
	void SaveImage(const std::string& filename);

	// Image properties
	GLint GetImageWidth() const { return GetImageWidth(0); }
	GLint GetImageHeight() const { return GetImageHeight(0); }
	GLint GetImageWidth(GLint level) const;
	GLint GetImageHeight(GLint level) const;
};


#endif