#include "GLImage.h"
#include "Assert.h"

//-----------------------------------------------------------------------------
U32 GLImage::GetPixelSize(GLuint format, GLuint type)
{
	U32 size = 1;

	switch (type) {
	case GL_UNSIGNED_BYTE:
		size = sizeof(GLubyte);
		break;
	case GL_FLOAT:
		size = sizeof(GLfloat);
		break;
	default:
		FAIL("Type not (yet) supported");
	}
	return size * GetChannelCount(format);
}
//-----------------------------------------------------------------------------
U32 GLImage::GetChannelCount(GLuint format)
{
	switch (format) {
	case GL_RGB:
		return 3;
	case GL_RED:
		return 1;
	default:
		FAIL("Format not (yet) supported");
	}
	return 0;
}
//-----------------------------------------------------------------------------
