#ifndef GLRESOURCE_H
#define GLRESOURCE_H

#include "OpenGL.h"

struct GLResource {
	GLResource();
	~GLResource();

	GLuint handle;
};

#endif