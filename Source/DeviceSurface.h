#ifndef DEVICESURFACE_H
#define DEVICESURFACE_H

#include "Entity.h"
#include "Mesh.h"
#include "ClassDecls.h"
#include "GLVertexArray.h"

struct DeviceSurface : public Entity<DeviceSurface, 1024> {
	DECL_NOT_COPYABLE(DeviceSurface)
public:
	DeviceSurface(const Mesh& mesh);
	~DeviceSurface();
	void Draw(GLenum mode);

	GLVertexArray vertexArray;
	GLAttribBufferPos3f positionBuffer;
	GLsizei vertexCount;
};

#endif