#include "VoxelGrid.h"

VoxelGrid gVoxelGrid;

//-----------------------------------------------------------------------------
void VoxelGrid::Init(const glm::vec3& center, GLfloat extent, 
	GLsizei cellCount, GLenum internalFormat)
{
	this->center = center;
	this->extent = extent;
	this->cellCount = cellCount;
	texture.Init();
	texture.SetImage(internalFormat, GLImage3D(cellCount, cellCount, 
		cellCount, GL_RED, GL_FLOAT));
	GL_ASSERT_SUCCESS();
}
//-----------------------------------------------------------------------------
void VoxelGrid::Deinit()
{
	texture.Deinit();
}
//-----------------------------------------------------------------------------
